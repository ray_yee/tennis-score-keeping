// Game Scores
const TennisScore = ['0', '15', '30', '40'];

//Constructor of Match
class Match {
  constructor(name1, name2) {
    this.players = [name1, name2];
    this.matchScore = [0, 0];
    this.gameScore = [0, 0];
    this.tieBreak = false;
    this.matchWinner = null;
  }

  // Function to display who are the players in the match
  playersIntro() {
    console.log(
      `This match is between ${this.players[0]} and ${this.players[1]}`
    );
  }

  // Function that manages the point calculation and display of results
  pointWonBy(name) {
    const playerIndex = this.players.findIndex(
      (playerName) => playerName === name
    );
    if (playerIndex === -1) {
      console.log('Invalid player');
    }
    if (!this.matchWinner) {
      this.addPoints(playerIndex);
      if (!this.tieBreak) {
        if (this.checkGameEnd()) {
          this.resetGameScore();
        }
        if (this.checkMatchEnd()) {
          console.log('Match has ended');
        }
        if (this.checkTieBreak()) {
          this.tieBreak = true;
        }
      } else {
        if (this.checkTieBreakEnd()) {
          console.log('Match has ended');
          this.resetGameScore();
        }
      }
    } else if (this.matchWinner) {
      console.log('Match has ended, please start a new match');
    }
  }

  // Function that adds the score base on the player index
  addPoints(playerIndex) {
    this.gameScore[playerIndex] += 1;
  }

  // Function that check whether Game has ended for normal games
  checkGameEnd() {
    if (this.gameScore[0] >= 4 || this.gameScore[1] >= 4) {
      const scoreDiff = this.gameScore[0] - this.gameScore[1];
      if (scoreDiff > 1) {
        this.matchScore[0] += 1;
        return true;
      }
      if (scoreDiff < -1) {
        this.matchScore[1] += 1;
        return true;
      }
    }
    return false;
  }

  // Function that checks whether Game and Match has ended for tie-break games
  checkTieBreakEnd() {
    if (this.gameScore[0] >= 7 || this.gameScore[1] >= 7) {
      const gameDiff = this.gameScore[0] - this.gameScore[1];
      if (gameDiff > 1) {
        this.matchWinner = this.players[0];
        return true;
      }
      if (gameDiff < -1) {
        this.matchWinner = this.players[1];
        return true;
      }
    }
    return false;
  }

  // Function that checks whether Match has ended
  checkMatchEnd() {
    if (this.matchScore[0] >= 6 || this.matchScore[1] >= 6) {
      const matchDiff = this.matchScore[0] - this.matchScore[1];
      if (matchDiff > 1) {
        this.matchWinner = this.players[0];
        return true;
      }
      if (matchDiff < -1) {
        this.matchWinner = this.players[1];
        return true;
      }
    }
    return false;
  }

  // Function that checks whether there is a Tie-break
  checkTieBreak() {
    if (this.matchScore[0] === 6 && this.matchScore[1] === 6) {
      return true;
    }
    return false;
  }

  // Function to reset the Game Score to 0-0
  resetGameScore() {
    this.gameScore = [0, 0];
  }

  // Function to display Match Score
  getMatchScore() {
    if (this.matchWinner) {
      return `Match won by ${this.matchWinner}`;
    }
    return `${this.matchScore[0]}-${this.matchScore[1]}`;
  }

  // Function to display Game Score
  getGameScore() {
    //Display the correct scoring base on whether there is tie-break
    const player1Score = this.tieBreak
      ? this.gameScore[0]
      : TennisScore[this.gameScore[0]];

    const player2Score = this.tieBreak
      ? this.gameScore[1]
      : TennisScore[this.gameScore[1]];

    if (!this.tieBreak) {
      if (this.gameScore[0] >= 3 && this.gameScore[1] >= 3) {
        if (this.gameScore[0] === this.gameScore[1]) {
          return 'Deuce';
        }
        if (this.gameScore[0] >= this.gameScore[1]) {
          return `Advantage ${this.players[0]}`;
        }
        if (this.gameScore[1] >= this.gameScore[0]) {
          return `Advantage ${this.players[1]}`;
        }
      }
    }
    return `${player1Score}-${player2Score}`;
  }

  // Function that display the current score of the match
  score() {
    console.log(
      `${this.getMatchScore() === '6-6' ? 'Tie-Break' : this.getMatchScore()}${
        this.getGameScore() === '0-0' ? '' : `, ${this.getGameScore()}`
      }`
    );
  }
}

const match = new Match('player 1', 'player 2');

match.playersIntro();

// Code that makes 'player 1' wins 5 matches
for (let i = 0; i < 5; i++) {
  match.pointWonBy('player 1');
  match.pointWonBy('player 2');
  match.score();

  match.pointWonBy('player 1');
  match.pointWonBy('player 1');
  match.score();

  match.pointWonBy('player 2');
  match.pointWonBy('player 2');
  match.score();

  match.pointWonBy('player 1');
  match.score();

  match.pointWonBy('player 1');
  match.score();
}

// Code that makes 'player 2' wins 5 matches
for (let i = 0; i < 5; i++) {
  match.pointWonBy('player 2');
  match.pointWonBy('player 1');
  match.score();

  match.pointWonBy('player 2');
  match.pointWonBy('player 2');
  match.score();

  match.pointWonBy('player 1');
  match.pointWonBy('player 1');
  match.score();

  match.pointWonBy('player 2');
  match.score();

  match.pointWonBy('player 2');
  match.score();
}

// Code that brings Match to a Tie-breaker

match.pointWonBy('player 2');
match.pointWonBy('player 2');
match.pointWonBy('player 2');
match.pointWonBy('player 2');
match.score();

match.pointWonBy('player 1');
match.pointWonBy('player 1');
match.pointWonBy('player 1');
match.pointWonBy('player 1');
match.score();

// Code that show the Tie-Breaker and Match End
match.pointWonBy('player 2');
match.pointWonBy('player 1');
match.pointWonBy('player 2');
match.pointWonBy('player 1');
match.pointWonBy('player 2');
match.pointWonBy('player 1');
match.pointWonBy('player 2');
match.pointWonBy('player 1');
match.pointWonBy('player 1');
match.pointWonBy('player 1');
match.score();
match.pointWonBy('player 1');
match.score();
