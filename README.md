# Tennis Score Keeping Challenge


### Run the program
```bash
node tennis.js
```

On running the program, a match is being created with the player's name 'player 1' and 'player 2'. The match shows 'player 1' winning for the first 5 games followed by 'player 2' winning the next 5. Subsequently, 'player 1' and 'player 2' each win 1 game to form a 'Tie-break'. The 'Tie-break' features a unique scoring system with the game ending when 'player 1' win.